// include gulp
var gulp = require('gulp'); 
 
// include plug-ins
var watch = require('gulp-watch');
var gulpShopify = require('gulp-shopify-upload');

//Report Errors to Console

gulp.on('error', function(e) {
  throw(e);
});

gulp.task('shopifywatch', function() {
    return watch('./+(assets|layout|config|snippets|templates|locales)/**')
    .pipe(gulpShopify('473077c52c76934ddb8851c7b49ce91d', 'e1d1e1765455e5beeb3a279eb22b833a', 'dream-bathrooms.myshopify.com', null, {}));
});

// Default gulp action when gulp is run
gulp.task('default', [
        'shopifywatch'
]);